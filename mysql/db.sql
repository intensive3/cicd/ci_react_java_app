CREATE USER 'admin'@'mysql' IDENTIFIED BY 'password';
CREATE DATABASE fullstack;
GRANT SELECT ON `fullstack`.* TO 'admin'@'mysql';
FLUSH PRIVILEGES;
